package main

import (
	"dapper-go/api"
	"dapper-go/args"
	"dapper-go/db"
	"dapper-go/watcher"
	"fmt"
	"log"

	// NOTE: enable for profiling
	// _ "net/http/pprof"
	// echopprof "github.com/sevenNt/echo-pprof"

	"github.com/sirupsen/logrus"
)

func main() {
	// setup logging
	logrus.SetFormatter(&logrus.TextFormatter{
		FullTimestamp: true,
	})

	// setup flags
	args.Setup()

	// setup database instance
	db := db.Instance()
	db.Path = *args.List.Database
	db.Read()

	err := watchForDatabaseChanges()
	if err != nil {
		log.Fatal(err)
	}

	// start HTTP server
	server := api.BuildServer()

	// NOTE: enable for profiling
	// echopprof.Wrap(server)

	err = server.Start(fmt.Sprintf(":%d", *args.List.Port))
	log.Fatalln(err)
}

func watchForDatabaseChanges() error {
	changeChan := make(chan bool)

	// wait for a message to be sent on the channel, then reload the database
	go func() {
		for {
			<-changeChan
			db.Instance().Read()
		}
	}()

	// watch database file for changes
	go watcher.WatchPathForChanges(db.Instance().Path, changeChan)

	return nil
}
