package codegen

import (
	"dapper-go/db"
	"dapper-go/errors"
	"io/ioutil"
	"mime/multipart"
	"os"

	"github.com/cbroglie/mustache"
)

// RenderTemplate renders a given Mustache template to a string
func RenderTemplate(upload *multipart.FileHeader) (string, error) {
	file, err := upload.Open()
	if err != nil {
		return "", errors.GetErrorByCodeWithOriginal(errors.TemplateReadFailed, err)
	}

	contents, err := ioutil.ReadAll(file)
	if err != nil {
		return "", errors.GetErrorByCodeWithOriginal(errors.TemplateReadFailed, err)
	}

	template, err := mustache.ParseString(string(contents))
	if err != nil {
		return "", errors.GetErrorByCodeWithOriginal(errors.TemplateRenderFailed, err)
	}

	data := codegen{
		Entries: []codegenEntry{},
	}

	entries := db.Instance().GetEntries()
	codes := db.Instance().SortedCodes()

	for _, code := range codes {
		code := db.EntryCode(code)
		entry := entries[code]
		entry.Code = code

		data.Entries = append(data.Entries, codegenEntry{
			Constants: codegenConstant{
				Go:  constantizeGo(entry),
				PHP: constantizePHP(entry),
			},
			Entry:     entry,
			EntryJSON: entry.ToJSON(),
		})
	}

	tmpFile, err := ioutil.TempFile(os.TempDir(), "dapper-")
	if err != nil {
		return "", errors.GetErrorByCodeWithOriginal(errors.RenderedTemplateStorageFailed, err)
	}

	output, err := template.Render(data)
	if err != nil {
		return "", errors.GetErrorByCodeWithOriginal(errors.TemplateRenderFailed, err)
	}

	_, err = tmpFile.Write([]byte(output))
	if err != nil {
		return "", errors.GetErrorByCodeWithOriginal(errors.RenderedTemplateStorageFailed, err)
	}

	return tmpFile.Name(), nil
}

type codegen struct {
	Entries []codegenEntry
}

type codegenEntry struct {
	Constants codegenConstant
	Entry     *db.Entry
	EntryJSON string
}

type codegenConstant struct {
	Go  string
	PHP string
}
