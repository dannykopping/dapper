package test

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gopkg.in/yaml.v2"
)

func ReadBodyAsBytes(response *http.Response) ([]byte, error) {
	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}

func ReadBodyAsString(response *http.Response) (string, error) {
	body, err := ReadBodyAsBytes(response)
	if err != nil {
		return "", err
	}

	return string(body), nil
}

func MarshalJSONRequest(request interface{}) (string, error) {
	json, err := json.MarshalIndent(request, "", "  ")
	if err != nil {
		return "", err
	}

	return string(json), nil
}

func UnmarshalJSONResponse(response *http.Response, target interface{}) error {
	body, err := ReadBodyAsBytes(response)
	if err != nil {
		return err
	}

	err = json.Unmarshal(body, target)
	if err != nil {
		return err
	}

	return nil
}

func UnmarshalYAMLResponse(response *http.Response, target interface{}) error {
	body, err := ReadBodyAsBytes(response)
	if err != nil {
		return err
	}

	err = yaml.Unmarshal(body, target)
	if err != nil {
		return err
	}

	return nil
}
