package test

type ErrorEntry struct {
	Code    string `yaml:"code,omitempty" json:"code,omitempty"`
	Message string `yaml:"message,omitempty" json:"message,omitempty"`
}

type ErrorResponse struct {
	Error ErrorDetail `yaml:"error"`
}

type ErrorDetail struct {
	Cause    string `yaml:"cause"`
	Code     string `yaml:"code"`
	Message  string `yaml:"message"`
	Solution string `yaml:"solution"`
}
