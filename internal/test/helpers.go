package test

import (
	"time"
)

func StartDapper(basePath string) (*Runner, error) {
	runner := NewRunner(basePath)
	if err := runner.Start(basePath); err != nil {
		return nil, err
	}

	if err := runner.WaitForStartup(2 * time.Second); err != nil {
		return nil, err
	}

	return runner, nil
}

func StopDapper(runner *Runner) {
	runner.Stop()
}
