package tests

import (
	"bytes"
	"dapper-go/errors"
	. "dapper-go/internal/test"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"strings"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("API", func() {
	var (
		dapper *Runner
		err    error
	)

	defer GinkgoRecover()

	BeforeEach(func() {
		dapper, err = StartDapper(BasePath)
		Expect(err).NotTo(HaveOccurred())
	})

	AfterEach(func() {
		if dapper != nil {
			err = dapper.Stop()
			Expect(err).NotTo(HaveOccurred())
		}
	})

	UnmarshalDapperErrorResponse := func(response *http.Response) ErrorResponse {
		var value ErrorResponse
		err = UnmarshalYAMLResponse(response, &value)
		Expect(err).NotTo(HaveOccurred())

		return value
	}

	UpsertEntry := func(entry *ErrorEntry) (*http.Response, error) {
		jsonRequest, err := MarshalJSONRequest(&entry)
		Expect(err).NotTo(HaveOccurred())

		response, err := http.Post(dapper.URI("/entries/%v", entry.Code),
			"application/json",
			strings.NewReader(jsonRequest),
		)

		return response, err
	}

	RetrieveEntry := func(code string) *ErrorEntry {
		response, err := http.Get(dapper.URI("/entries/%v", code))
		Expect(err).NotTo(HaveOccurred())
		Expect(response.StatusCode).To(Equal(http.StatusOK))

		entryResponse := ErrorEntry{}
		err = UnmarshalYAMLResponse(response, &entryResponse)
		Expect(err).NotTo(HaveOccurred())

		return &entryResponse
	}

	MarkDatabaseReadOnly := func() {
		// make database file read-only, so the deletion cannot succeed
		err := os.Chmod(dapper.DatabaseFile.Name(), 0440)
		Expect(err).NotTo(HaveOccurred())
	}

	UploadFile := func(url string, filename string, fieldName string) (*http.Response, error) {
		client := &http.Client{}
		file, err := os.Open(filename)
		defer file.Close()

		Expect(err).NotTo(HaveOccurred())

		body := &bytes.Buffer{}
		writer := multipart.NewWriter(body)

		part, err := writer.CreateFormFile(fieldName, file.Name())
		Expect(err).NotTo(HaveOccurred())

		io.Copy(part, file)
		writer.Close()

		request, err := http.NewRequest("POST", dapper.URI("/generate"), body)
		request.Header.Add("Content-Type", writer.FormDataContentType())
		Expect(err).NotTo(HaveOccurred())

		return client.Do(request)
	}

	Describe("Status endpoint", func() {
		It("Should respond with the API version", func() {
			response, err := http.Get(dapper.URI("/status"))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusOK))

			var status struct {
				Version string `json:"version"`
			}

			if err := UnmarshalJSONResponse(response, &status); err != nil {
				Expect(err).NotTo(HaveOccurred())
			}

			Expect(status.Version).To(Not(BeNil()))
		})
	})

	Describe("A missing endpoint", func() {
		It("Should respond with a 404", func() {
			response, err := http.Get(dapper.URI("/missing"))
			Expect(err).NotTo(HaveOccurred())

			Expect(response.StatusCode).To(Equal(http.StatusNotFound))
		})
	})

	Describe("Entries endpoints", func() {

		Describe("Get all entries endpoint", func() {
			It("Should respond with a 200", func() {
				response, err := http.Get(dapper.URI("/entries"))
				Expect(err).NotTo(HaveOccurred())

				Expect(response.StatusCode).To(Equal(http.StatusOK))
			})

			It("Should respond with a list of 2 entries", func() {
				response, err := http.Get(dapper.URI("/entries"))
				Expect(err).NotTo(HaveOccurred())

				var entries map[string]struct{}
				err = UnmarshalYAMLResponse(response, &entries)
				Expect(err).NotTo(HaveOccurred())

				Expect(len(entries)).To(Equal(2))
			})
		})

		Describe("Get single entry endpoint", func() {
			It("Should respond with a 200 and the details of the entry", func() {
				response, err := http.Get(dapper.URI("/entries/resource_locked"))
				Expect(err).NotTo(HaveOccurred())

				Expect(response.StatusCode).To(Equal(http.StatusOK))
			})

			It("Should respond with at least a code and message", func() {
				errorCode := "resource_locked"
				response, err := http.Get(dapper.URI("/entries/%v", errorCode))
				Expect(err).NotTo(HaveOccurred())

				entry := ErrorEntry{}
				err = UnmarshalYAMLResponse(response, &entry)
				Expect(err).NotTo(HaveOccurred())

				Expect(entry.Message).To(Equal("Resource is locked"))
				Expect(entry.Code).To(Equal(errorCode))
			})

			It("Should respond with a 404/missing_entry if a code is not given", func() {
				response, err := http.Get(dapper.URI("/entries/"))
				Expect(err).NotTo(HaveOccurred())
				Expect(response.StatusCode).To(Equal(http.StatusNotFound))

				errResponse := UnmarshalDapperErrorResponse(response)
				errorEntry := errors.GetErrorByCode(errors.MissingEntry)

				Expect(errResponse.Error.Code).To(BeEquivalentTo(errorEntry.Code))
			})

			It("Should respond with a 400/missing_code if the code given is blank", func() {
				blankCode := " " // value must be empty otherwise it will not hit the correct route
				response, err := http.Get(dapper.URI("/entries/%v", blankCode))
				Expect(err).NotTo(HaveOccurred())
				Expect(response.StatusCode).To(Equal(http.StatusBadRequest))

				errResponse := UnmarshalDapperErrorResponse(response)
				errorEntry := errors.GetErrorByCode(errors.MissingCode)

				Expect(errResponse.Error.Code).To(BeEquivalentTo(errorEntry.Code))
			})

			// TODO add validation for code - is it even necessary?
			// It("Should respond with a 400/invalid_code if the code given is not valid", func() {
			// 	response, err := http.Get(dapper.URI("/entries/A and B"))
			// 	Expect(err).NotTo(HaveOccurred())
			// 	Expect(response.StatusCode).To(Equal(http.StatusBadRequest))

			// 	errResponse := UnmarshalDapperErrorResponse(response)
			// 	errorEntry := errors.GetErrorByCode(errors.InvalidCode)

			// 	Expect(errResponse.Error.Code).To(BeEquivalentTo(errorEntry.Code))
			// })
		})

		Describe("Upsert entry endpoint", func() {
			It("Should respond with 200 when creating an entry and the entry must be retrieved correctly", func() {
				entry := ErrorEntry{
					Message: "this is a message",
					Code:    "error_identifier",
				}

				response, err := UpsertEntry(&entry)
				Expect(err).NotTo(HaveOccurred())
				Expect(response.StatusCode).To(Equal(http.StatusOK))

				entryResponse := RetrieveEntry(entry.Code)

				Expect(entryResponse.Code).To(BeEquivalentTo(entry.Code))
				Expect(entryResponse.Message).To(Equal(entry.Message))
			})

			It("Should respond with 200 when updating an existing entry and the entry must be retrieved changed", func() {
				entry := ErrorEntry{
					Code:    "resource_access_failed",
					Message: "Could not access resource!",
				}

				// check initial state
				entryResponse := RetrieveEntry(entry.Code)
				Expect(entryResponse.Message).To(Equal("Could not access resource"))

				// upsert entry and check new state
				response, err := UpsertEntry(&entry)

				Expect(err).NotTo(HaveOccurred())
				Expect(response.StatusCode).To(Equal(http.StatusOK))

				entryResponse = RetrieveEntry(entry.Code)

				Expect(entryResponse.Code).To(BeEquivalentTo(entry.Code))
				Expect(entryResponse.Message).To(Equal(entry.Message))
			})

			It("Should respond with 400/missing_request_parameter when creating an entry without specifying the required fields", func() {
				entry := ErrorEntry{
					Code: "will_not_work",
					// no message = validation error
				}

				response, err := UpsertEntry(&entry)
				Expect(err).NotTo(HaveOccurred())
				Expect(response.StatusCode).To(Equal(http.StatusBadRequest))

				errResponse := UnmarshalDapperErrorResponse(response)
				errorEntry := errors.GetErrorByCode(errors.MissingRequestParameters)

				Expect(errResponse.Error.Code).To(BeEquivalentTo(errorEntry.Code))
			})

			It("Should respond with 500/failed_upsert when upserting an entry but the database cannot be saved", func() {
				entry := ErrorEntry{
					Code:    "resource_access_failed",
					Message: "this will not work",
				}

				MarkDatabaseReadOnly()

				// upsert entry and check new state
				response, err := UpsertEntry(&entry)

				Expect(err).NotTo(HaveOccurred())
				Expect(response.StatusCode).To(Equal(http.StatusInternalServerError))

				errResponse := UnmarshalDapperErrorResponse(response)
				errorEntry := errors.GetErrorByCode(errors.FailedUpsert)

				Expect(errResponse.Error.Code).To(BeEquivalentTo(errorEntry.Code))
			})
		})

		Describe("Delete entry endpoint", func() {
			It("Should respond with 200 when deleting an entry successfully", func() {

				// weird, http client doesn't have `Delete` method
				client := &http.Client{}
				entryCode := 1000

				request, err := http.NewRequest(http.MethodDelete, dapper.URI("/entries/%v", entryCode), nil)
				Expect(err).NotTo(HaveOccurred())

				response, err := client.Do(request)
				Expect(err).NotTo(HaveOccurred())
				Expect(response.StatusCode).To(Equal(http.StatusOK))

				response, err = http.Get(dapper.URI("/entries/%v", 1000))
				Expect(err).NotTo(HaveOccurred())
				Expect(response.StatusCode).To(Equal(http.StatusNotFound))
			})

			It("Should respond with 200 when attempting to delete an entry that does not exist", func() {

				client := &http.Client{}
				entryCode := 9999

				request, err := http.NewRequest(http.MethodDelete, dapper.URI("/entries/%v", entryCode), nil)
				Expect(err).NotTo(HaveOccurred())

				response, err := client.Do(request)
				Expect(err).NotTo(HaveOccurred())
				Expect(response.StatusCode).To(Equal(http.StatusOK))
			})

			It("Should respond with 500/failed_delete when deleting an entry but the database cannot be saved", func() {

				client := &http.Client{}
				entryCode := 1000

				MarkDatabaseReadOnly()

				request, err := http.NewRequest(http.MethodDelete, dapper.URI("/entries/%v", entryCode), nil)
				Expect(err).NotTo(HaveOccurred())

				response, err := client.Do(request)
				Expect(err).NotTo(HaveOccurred())
				Expect(response.StatusCode).To(Equal(http.StatusInternalServerError))

				errResponse := UnmarshalDapperErrorResponse(response)
				errorEntry := errors.GetErrorByCode(errors.FailedDelete)

				Expect(errResponse.Error.Code).To(BeEquivalentTo(errorEntry.Code))
			})
		})

		Describe("Code generation endpoint", func() {
			It("Should respond with 200 and the generated code when generating code successfully", func() {

				response, err := UploadFile(dapper.URI("/generate"), BasePath+"/fixtures/codegen.template", "template")
				Expect(err).NotTo(HaveOccurred())
				Expect(response.StatusCode).To(Equal(http.StatusOK))

				renderedTemplateExpectation, err := ioutil.ReadFile(BasePath + "/fixtures/codegen.outcome")
				Expect(err).NotTo(HaveOccurred())

				Expect(ReadBodyAsString(response)).To(Equal(string(renderedTemplateExpectation)))
			})

			It("Should respond with 400/missing_request_parameters when no 'template' form parameter is provided", func() {

				response, err := http.Post(dapper.URI("/generate"), "text/plain", nil)
				Expect(err).NotTo(HaveOccurred())
				Expect(response.StatusCode).To(Equal(http.StatusBadRequest))

				errResponse := UnmarshalDapperErrorResponse(response)
				errorEntry := errors.GetErrorByCode(errors.MissingRequestParameters)

				Expect(errResponse.Error.Code).To(BeEquivalentTo(errorEntry.Code))
			})
		})
	})
})
