package watcher

import (
	"dapper-go/errors"
	"fmt"

	"github.com/sirupsen/logrus"
	"gopkg.in/fsnotify.v1"
)

// TODO clean this up (get rid of inner anonymous function)

// WatchPathForChanges watches a given path for writes
func WatchPathForChanges(path string, onChange chan bool) {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		watchErrorHandler(path, err)
	}
	defer watcher.Close()

	done := make(chan bool)

	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					watchErrorHandler(path, fmt.Errorf("failed reading from watcher channel"))
					return
				}

				// TODO when the file is written, its inode is replaced with a new one,
				// and as such we receive 2 "write" events: one for clearing the file
				// and another for writing the new contents
				// - consider using modified time to prevent sending twice
				if event.Op&fsnotify.Write == fsnotify.Write {
					onChange <- true
				}
			case err, ok := <-watcher.Errors:
				if !ok {
					watchErrorHandler(path, err)
					return
				}
			}
		}
	}()

	err = watcher.Add(path)
	if err != nil {
		watchErrorHandler(path, err)
	}
	<-done
}

func watchErrorHandler(path string, err error) {
	errorEntry := errors.GetErrorByCodeWithOriginal(errors.DbUnwatchable, err)
	errorEntry.Message = fmt.Sprintf(errorEntry.Message, path)
	logrus.WithFields(logrus.Fields{
		"original_error": err,
	}).Fatalln(errorEntry)
}
