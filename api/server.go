package api

import (
	"dapper-go/codegen"
	"dapper-go/db"
	"dapper-go/errors"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gopkg.in/yaml.v2"
)

const Version string = "1.0.0"

// BuildServer creates a new HTTP server
func BuildServer() *echo.Echo {

	server := echo.New()
	server.Use(middleware.Recover())

	server.HideBanner = true
	server.HTTPErrorHandler = handleError

	/* API */
	server.GET("/status", statusHandler)
	server.GET("/entries", getAllEntriesHandler)
	server.GET("/entries/", missingEntryHandler)
	server.GET("/entries/:code", getEntryHandler)
	// server.GET("/entries/:code/:translation", getEntryByLocaleHandler)
	server.POST("/entries/:code", upsertEntryHandler)
	server.PUT("/entries/:code", upsertEntryHandler)
	server.PATCH("/entries/:code", upsertEntryHandler)
	server.DELETE("/entries/:code", deleteEntryHandler)

	server.POST("/generate", codeGenerationHandler)

	return server
}

func sendYAML(c echo.Context, status uint16, data interface{}) error {
	content, err := yaml.Marshal(data)
	if err != nil {
		return errors.GetErrorByCodeWithOriginal(errors.SerializationFailed, err)
	}

	return c.Blob(http.StatusOK, "application/yaml", content)
}

func statusHandler(c echo.Context) (err error) {
	return c.JSON(http.StatusOK, struct {
		Version string `json:"version"`
	}{
		Version: Version,
	})
}

func getAllEntriesHandler(c echo.Context) (err error) {
	return sendYAML(c, http.StatusOK, db.Instance().GetEntries())
}

func missingEntryHandler(c echo.Context) (err error) {
	return errors.GetErrorByCode(errors.MissingEntry)
}

func getEntryHandler(c echo.Context) (err error) {
	code, err := getCodeParam(c)
	if err != nil {
		return err
	}

	entry, err := db.Instance().GetEntryByCode(code)
	if err != nil {
		return errors.GetErrorByCodeWithOriginal(errors.MissingEntry, err)
	}

	// TODO exclude translations from output?
	return sendYAML(c, http.StatusOK, entry)
}

// Handles inserting/updating of an entry
func upsertEntryHandler(c echo.Context) (err error) {
	code, err := getCodeParam(c)
	if err != nil {
		return err
	}

	// fetch existing entry
	entry, err := db.Instance().GetEntryByCode(code)
	if err != nil {
		// no entry found - create new blank one
		entry = &db.Entry{
			Code:      code,
			CreatedAt: time.Now().Unix(),
		}
	}

	// overwrite with properties sent via API
	if err = c.Bind(&entry); err != nil {
		return errors.GetErrorByCodeWithOriginal(errors.ParameterBind, err)
	}

	// check if the entry has all the required fields
	if invalidFields := entry.InvalidFields(); invalidFields != nil {
		errorEntry := errors.GetErrorByCode(errors.MissingRequestParameters)
		errorEntry.Message = fmt.Sprintf(errorEntry.Message, invalidFields)
		return errorEntry
	}

	// save to database
	if err = db.Instance().UpsertEntry(code, entry); err != nil {
		return errors.GetErrorByCodeWithOriginal(errors.FailedUpsert, err)
	}

	return c.NoContent(http.StatusOK)
}

func deleteEntryHandler(c echo.Context) (err error) {
	code, err := getCodeParam(c)
	if err != nil {
		return err
	}

	err = db.Instance().DeleteEntry(code)
	if err != nil {
		return errors.GetErrorByCodeWithOriginal(errors.FailedDelete, err)
	}

	return c.NoContent(http.StatusOK)
}

func codeGenerationHandler(c echo.Context) (err error) {
	upload, err := c.FormFile("template")
	if err != nil {
		entry := errors.GetErrorByCodeWithOriginal(errors.MissingRequestParameters, err)
		entry.Message = fmt.Sprintf(entry.Message, "template")
		return entry
	}

	outputFilename := c.QueryParam("output-filename")
	if len(strings.TrimSpace(outputFilename)) <= 0 {
		outputFilename = "generated.txt"
	}

	tmpFile, err := codegen.RenderTemplate(upload)
	if err != nil {
		return err
	}

	// ensure that tmpfile is removed afterwards
	defer os.Remove(tmpFile)

	c.Response().Header().Set(echo.HeaderContentType, "text/plain; charset=UTF-8")
	c.Attachment(tmpFile, outputFilename)
	return err
}

// func getEntryByLocaleHandler(c echo.Context) (err error) {
// 	code, err := getCodeParam(c)
// 	translation := c.Param("translation")
// 	if err != nil {
// 		return err
// 	}

// 	entry, err := db.Instance().GetTranslatedEntryByCode(code, translation)
// 	if err != nil {
// 		return c.NoContent(http.StatusNotFound)
// 	}

// 	// TODO exclude translations from output
// 	return sendYAML(c, http.StatusOK, entry)
// }

func getCodeParam(c echo.Context) (db.EntryCode, error) {
	codeParam := c.Param("code")
	if len(strings.TrimSpace(codeParam)) <= 0 {
		return "", errors.GetErrorByCode(errors.MissingCode)
	}

	return db.EntryCode(codeParam), nil
}
