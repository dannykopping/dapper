package db

import (
	"dapper-go/errors"
	"dapper-go/helpers"
	"encoding/json"
	"time"

	"github.com/sirupsen/logrus"
	"gopkg.in/guregu/null.v4"
)

// Database describes the contents of the entries database
type Database struct {
	Path             string `yaml:"-" json:"-"` // don't include this in serialized output
	DatabaseContents `yaml:",inline"`
}

type DatabaseContents struct {
	Version uint16               `yaml:"version" json:"version"`
	Entries map[EntryCode]*Entry `yaml:"entries" json:"entries"`
}

// EntryContents is the struct of common contents for entries and their translations
type EntryContents struct {
	Message     string                 `yaml:"message" json:"message"`
	Description null.String            `yaml:"description" json:"description"`
	Cause       null.String            `yaml:"cause" json:"cause"`
	Solution    null.String            `yaml:"solution" json:"solution"`
	Meta        map[string]interface{} `yaml:"meta" json:"meta"`
	Tags        []string               `yaml:"tags" json:"tags"`
}

// Entry describes the structure of an entry in the database
type Entry struct {
	EntryContents `yaml:",inline"`

	Code      EntryCode `yaml:"code" json:"code"`
	CreatedAt int64     `yaml:"created" json:"created"`
	UpdatedAt int64     `yaml:"updated" json:"updated"`
	// Translations map[TranslationCode]EntryTranslation `yaml:"translations" json:"translations"`
}

// EntryTranslation describes the structure of an entry translation
type EntryTranslation struct {
	EntryContents `yaml:",inline"`
}

// EntryCode describes all error codes
type EntryCode string

// TranslationCode represents an entry's translation identifier
// type TranslationCode string

// UnixTimestamp holds unix timestamp values
type UnixTimestamp uint64

func (u UnixTimestamp) ToDateTime(format string) string {
	t := time.Unix(int64(u), 0)
	return t.Format(format)
}

func (u UnixTimestamp) Valid() bool {
	return uint64(u) > 0
}

func (entry *Entry) InvalidFields() []string {
	var invalidFields []string

	// TODO we probably don't need this method anymore
	if helpers.EmptyString(string(entry.Message)) {
		invalidFields = append(invalidFields, "message")
	}

	return invalidFields
}

func (entry *Entry) ApplyTranslation(translation EntryTranslation) *Entry {
	if len(translation.Message) > 0 {
		entry.Message = translation.Message
	}

	return entry
}

func (entry *Entry) ToJSON() string {
	contents, err := json.Marshal(entry)
	if err != nil {
		errorEntry := errors.GetErrorByCodeWithOriginal(errors.SerializationFailed, err)
		logrus.Warningln(errorEntry)

		return "{\"message\": \"could not serialize entry to JSON\"}"
	}

	return string(contents)
}
